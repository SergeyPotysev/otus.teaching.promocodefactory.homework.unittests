﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using FluentAssertions.Extensions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ThrowsNotFoundException()
        {
            // Arrange
            Partner partner = null;
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);
 
            // Assert
            response.Should().BeOfType<NotFoundResult>();
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ThrowsBadRequestException()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase().SetActiveStatus(false);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, null);
 
            // Assert
            response.Should().BeOfType<BadRequestObjectResult>();
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfTheLimitIsNotOver_ResetPromoCodeCounter()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase().SetNumberIssuedPromoCodes(123);
            var request = PartnerPromoCodeLimitBuilder.CreateBase().SetEndDate(30).SetLimit(50);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
 
            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
   
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfTheLimitIsOver_DoNotResetPromoCodeCounter()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase().SetCancelDate().SetNumberIssuedPromoCodes(123);
            var request = PartnerPromoCodeLimitBuilder.CreateBase().SetEndDate(30).SetLimit(50);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
 
            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(123);
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfTheLimitIsNotOver_TurnOffLastLimit()
        {
            // Arrange
            DateTime cancelDate = default;
            var partner = PartnerBuilder.CreateBase();
            var request = PartnerPromoCodeLimitBuilder.CreateBase().SetEndDate(30).SetLimit(50);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                x.CancelDate.HasValue);
            if (activeLimit?.CancelDate != null)
                cancelDate = (DateTime) activeLimit.CancelDate;

            // Assert
            DateTime.Now.Should().BeLessThan(1.Seconds()).Before(target: cancelDate);
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsNegative_ThrowsBadRequestException()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase();
            var request = PartnerPromoCodeLimitBuilder.CreateBase().SetEndDate(30).SetLimit(-20);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
 
            // Assert
            response.Should().BeOfType<BadRequestObjectResult>();
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfSavedToDataBase_UpdateMethodCalledOnce()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBase();
            var request = PartnerPromoCodeLimitBuilder.CreateBase().SetEndDate(30).SetLimit(10);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var response = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
 
            // Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }

    }
}