﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public static class PartnerBuilder
    {
        public static Partner CreateBase()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
        }
    
    
        public static Partner SetActiveStatus(this Partner partner, bool isActive)
        {
            partner.IsActive = isActive;
            return partner;
        }

    
        public static Partner SetId(this Partner partner, string guid)
        {
            partner.Id = Guid.Parse(guid);
            return partner;
        }
        
        
        public static Partner SetCancelDate(this Partner partner)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
            }
  
            return partner;
        }
        
        
        public static Partner SetNumberIssuedPromoCodes(this Partner partner, int numberIssuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;
            return partner;
        }
    }
}