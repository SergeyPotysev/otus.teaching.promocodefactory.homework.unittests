﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public static class PartnerPromoCodeLimitBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBase()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
            };
        }
        
        
        public static SetPartnerPromoCodeLimitRequest SetEndDate(this SetPartnerPromoCodeLimitRequest request, int addDays)
        {
            request.EndDate = DateTime.Now.AddDays(addDays);
            return request;
        }

        
        public static SetPartnerPromoCodeLimitRequest SetLimit(this SetPartnerPromoCodeLimitRequest request, int limit)
        {
            request.Limit = limit;
            return request;
        }
    }
}
